#!/usr/bin/env python3
import requests
import sys
import json

#  read in files from command line arguments
if len(sys.argv) < 4:
    print('Incorrect command format.\nUSAGE: create_microreact.py <MICROREACT ACCESS TOKEN> <MICROREACT CSV> <NEWICK FILE>')
    sys.exit(1)

access_token = sys.argv[1]
microreact_csv = sys.argv[2]
newick_tree = sys.argv[3]

with open(microreact_csv) as file:
    data_string = file.read()

with open(newick_tree) as file:
    tree_string = file.read()

# create new microreact
headers = {
    'Content-type': 'application/json; charset=utf-8',
    'Access-Token': access_token,
}   

data = {
    "name": "SARS-CoV-2",
    "data": data_string,
    "tree": tree_string
}

# Convert data to microreact json format
data_json = json.dumps(data, indent=4)
response = requests.post('https://demo.microreact.org/api/schema/convert',
    headers=headers,
    data=data_json
)
microreact_json = response.text # response returns valid json

# Create project
response = requests.post('https://microreact.org/api/projects/create/',
    headers=headers,
    data=microreact_json
)
json_response = json.loads(response.text)
print(json_response['url'])