[![logo_agrosavia](/logos/logo_agrosavia.png)](https://www.agrosavia.co/nosotros/grupos-de-investigaci%C3%B3n/investigaci%C3%B3n-y-vigilancia-integrada-de-la-resistencia-antimicrobiana)       [![logo_KIMS](/logos/Kims-01.png)](http://www.kimsbangalore.edu.in/) 
[![logo_OXF_UK](/logos/OXF_UK.png)](https://ghru.pathogensurveillance.net/)

## Nextflow Pipeline SARS-CoV-2 Visualizer

This process generate an intelligent visualization of phylogeny inference, lineages, clades and VOCs of SARS-CoV2 based on genome mapping to reference Wuhan_Hu_1, quality filtering (min_cov and min_lenght), tree generation (fasttree default; iqtree alternative) and pangolin, nextclade and type_variants prediction software. Nextflow and Docker infrastructure makes the process scalable, reproducible, and easy to use!!

### Pre-requisites:

 * conda `https://docs.conda.io/en/latest/miniconda.html`
 or
 * Docker `https://docs.docker.com/get-docker`

### Conda **maintaining** 

If using conda create an environment named cov2
```
conda env create -f Docker/conda_environments/environment.yml
```
If conda is already install, please, check conda env path in nextflow.config file `-profile conda`

### Docker

If using docker, pull sarscov2 container
```
docker pull registry.gitlab.com/johan.bernal.morales/sarscov2:latest
```

#### Software included in Conda environment or Docker container

 * minimap2 2.17
 * datafunk 0.0.8
 * clusterfunk 0.0.3
 * pangolin 3.0.5
 * fasttree 2.1.10
 * iqtree 2.0.3
 * type_variants
 * nextclade 0.14.4

### Running the workflow

```
nextflow run main.nf --fasta_file <PATH TO SARS-CoV-2 CONSENSUS FASTAS> \
                     --config_dir $PWD/type_variants/config.csv \
                     --output_dir <OUTPUT DIRECTORY> \
                     --tree \
                     --iqtree \
                     -resume \
                     -profile <conda or docker or test>
```
The <PATH TO SARS-CoV-2 CONSENSUS FASTAS> can either be
  * the path to a multifasta file such as `/path/input_dir/multi_consensus.fasta`
  * a glob file pattern match such as `"/path/input_dir/*.fasta"`

 Tree building is optional using `--tree` parameter, you could use fasttree (default) or `--iqtree` option

OR edit run_coV2_test.sh

### Making Microreact projects

To create a microreact-compatible csv provide a csv file with the `--microreact_metadata <PATH TO METADATA>` parameter in `run_coV2_test.sh`. This file should have a column `id` where the values correspond to the fasta file heads e.g
 * header is 
`>MT126808.1 Severe acute respiratory syndrome coronavirus 2 isolate SARS-CoV-2/human/BRA/SP02/2020, complete genome` 
 * id is `MT126808.1`

Additional columns can be added such as 
 * `longitude` and `latitute` to add a location
 * `day`, `month`, and `year` to add dates to a timeline for the samples
 * any other relevant data!!

To create a Microreact project from the compatible csv add the access_token for your account using the `--microreact_access_token <Your API access token>` parameter in `run_coV2_test.sh`.

#### Automatic full colouring SARS-CoV-2 report looks like this!!  

* #### National report 

[![national_report](/logos/report_example.png)](https://microreact.org/project/3g49U1D9WkQD6tgeknZ7hP/0f175c7b)

* #### Regional report

![regional_report](/logos/report_example_1.png)



## Pipeline scheme

![pipeline_scheme](/logos/pipeline.png)

## Authors:
[Johan Fabian Bernal](https://gitlab.com/johan.bernal.morales) <johan.bernal.morales@gmail.com> <br>
[Varun Shamanna](https://gitlab.com/varunshamanna4) <varunshamanna4@gmail.com> <br>
[Anthony Underwood](https://gitlab.com/antunderwood) <au3@sanger.ac.uk> <br>

***Johan F. Bernal*** is microbiologist with (c)master’s degree in public health from Andes University Bogotá, Colombia. He has been working approximately for 10 years from "one health" scope generating knowledge in antimicrobial resistance and molecular epidemiology of infectious diseases in Colombia. Last 5 years, he has been part of the implementation of genomics to understand antimicrobial resistance and the epidemiology in pathogens of global health concern. 

His working base has been “Corporación Colombiana de Investigación Agropecuaria-AGROSAVIA" involved in the Colombian program for antimicrobial resistance surveillance- COIPARS. He had participated in national and international projects, recently, he is part of NIH Global Health Research Unit, initiative of the Centre for Genomic Pathogen Surveillance (CGPS) at Sanger institute in UK, and for 2 years, he has been bioinformatic area coordinator in AGROSAVIA. He has been involved in different OMS/PAHO pathogens reference networks in latin america (Pulsenet, Relavra).

***Varun Shamanna*** joined the lab soon after his Mtech and continues working on implementing major Bioinformatics pipelines in the lab. He is a passionate researcher with deep expertise in  Linux and Windows operating systems as well as knowledge in network management. He is someone who is agile and adaptable, with the ability to think quickly and deliver. During his tenure at CRL, he has undertaken multiple projects on high throughput analysis of Whole Genome Sequence data of Streptococcus pneumoniae, Klebsiella pneumoniae, and Staphylococcus aureus. He performed a comparison of tools and design of sophisticated pipeline for metagenomic data analysis as part of his Masters project.
