FROM continuumio/miniconda3:4.10.3
LABEL authors="gitlab@:JohanFBernal;varumshamanna4; antunderwoood" \
      description="Docker image for Assigning Lineages to SARS_COV2"

RUN apt update; apt install -y  gcc procps

RUN conda install mamba -n base -c conda-forge

COPY Docker/conda_environments/environment.yml /
RUN mamba env create -f /environment.yml && conda clean -a

# copy scripts in place
COPY Docker/scripts/make_metadata.py /usr/local/bin/
COPY Docker/scripts/create_microreact.py /usr/local/bin/
COPY Docker/scripts/type_variants.py /usr/local/bin/
RUN chmod +x /usr/local/bin/*.py

# remove conda activation
RUN sed -i 's/conda activate base//g' /root/.bashrc

ENV PATH /opt/conda/envs/cov2/bin:/opt/conda/bin:/opt/conda/condabin:$PATH
